all: wrapper.so wrapper.cdef.h

wrapper.so: wrapper.c wrapper.h
	clang -shared -lclang wrapper.c -o wrapper.so

wrapper.cdef.h: wrapper.h wrapper.so generate_cdef.lua generate_wrapper_cdef.lua secure_cdef.lua
	luajit generate_wrapper_cdef.lua > wrapper.cdef.new.h
	mv wrapper.cdef.h wrapper.cdef.old.h
	mv wrapper.cdef.new.h wrapper.cdef.h
