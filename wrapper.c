#include "wrapper.h"

enum CXChildVisitResult call_wrapper(CXCursor cursor, CXCursor parent, CXClientData client_data) {
  return ((wrapp_f)client_data)(&cursor);
}

void *wrap_function(wrapp_f f) {
  return (void *)f;
}
