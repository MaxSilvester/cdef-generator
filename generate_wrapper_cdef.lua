#!/usr/bin/env luajit

local generate_cdef = require("generate_cdef")

print(generate_cdef("wrapper.h", "-I.", {
  "clang_createIndex",
  "clang_createTranslationUnit",
  "clang_Cursor_getArgument",
  "clang_Cursor_getNumArguments",
  "clang_Cursor_isAnonymous",
  "clang_disposeString",
  "clang_getArgType",
  "clang_getArrayElementType",
  "clang_getCString",
  "clang_getCursorKind",
  "clang_getCursorKindSpelling",
  "clang_getCursorPrettyPrinted",
  "clang_getCursorResultType",
  "clang_getCursorSpelling",
  "clang_getCursorType",
  "clang_getNumArgTypes",
  "clang_getPointeeType",
  "clang_getResultType",
  "clang_getTranslationUnitCursor",
  "clang_getTypeDeclaration",
  "clang_getTypedefDeclUnderlyingType",
  "clang_getTypeSpelling",
  "clang_hashCursor",
  "clang_visitChildren",
  "call_wrapper",
  "wrap_function",
}))
