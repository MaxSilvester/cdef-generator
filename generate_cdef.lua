local ffi = require("ffi")
local secure_cdef = require("secure_cdef")

local clang
do
  local file = io.open("wrapper.cdef.h")
  local str = file:read("*a")
  --print(str)
  file:close()

  secure_cdef(str)

  clang = ffi.load("./wrapper.so")
end

local function visit_children(cursor, f)

  local callback = ffi.cast("wrapp_f", function(cursor)
    return f(cursor[0])
  end)
  clang.clang_visitChildren(cursor, clang.call_wrapper, clang.wrap_function(callback))
  callback:free()
end

local function get_ls(cxs)
  local ls = ffi.string(clang.clang_getCString(cxs))
  clang.clang_disposeString(cxs)
  return ls
end

local function get_ppd(cursor)
  return get_ls(clang.clang_getCursorPrettyPrinted(cursor, nil))
end

local function get_cs(cursor)
  return get_ls(clang.clang_getCursorSpelling(cursor))
end

local function generate_cdef(input_filenames, args, functions_filter_input)
  local tmp_input_filename = os.tmpname()
  do
    local files
    if type(input_filenames) == type({}) then
      files = input_filenames
    elseif input_filenames == nil then
      files = {}
    else
      files = {input_filenames}
    end

    local lines = {}
    for _, file in ipairs(files) do
      table.insert(lines, ("#include<%s>"):format(file))
    end

    local concatenated = table.concat(lines, "\n")

    local tmp_file = io.open(tmp_input_filename, "w+")
    tmp_file:write(concatenated)
    tmp_file:close()
  end
  local additional_args
  do
    if args == nil then
      additional_args = ""
    elseif type(args) == type("") then
      additional_args = args
    else
      additional_args = table.concat(args, " ")
    end
  end
  local functions_filter
  do
    local functions_filter_input = functions_filter_input
    if functions_filter_input == nil then
      functions_filter = nil
    else
      functions_filter = {}
      if type(functions_filter_input) == type("") then
        functions_filter_input = {functions_filter_input}
      end
      for _, i in ipairs(functions_filter_input) do
        functions_filter[i] = true
      end
    end
  end
  local tmp_filename = os.tmpname()
  --os.execute("cp "..input_filename.." /dev/shm/1")
  os.execute(("clang -x c -emit-ast -o %s %s %s"):format(tmp_filename, additional_args, tmp_input_filename))
  local index = clang.clang_createIndex(1, 1)
  local translation_unit = clang.clang_createTranslationUnit(index, tmp_filename)
  os.remove(tmp_filename)
  os.remove(tmp_input_filename)
  local cursor = clang.clang_getTranslationUnitCursor(translation_unit)

  local functions = {}
  local types = {}

  local added_types = {}

  local add_type_if_necessary

  local function get_typedef_string(cursor)
    --print(get_ppd(cursor))
    local declaration = get_ppd(cursor) .. ";"

    local cxtype = clang.clang_getTypedefDeclUnderlyingType(cursor)
    local backup_name = get_cs(cursor)
    add_type_if_necessary(cxtype, backup_name)

    return declaration
  end

  local function get_struct_field_declaration(cursor)
    local declaration = get_ppd(cursor) .. ";"

    add_type_if_necessary(clang.clang_getCursorType(cursor))

    return declaration
  end

  local function get_struct_string(cursor, backup_name)
    local declaration = "struct "

    local name = get_cs(cursor)
    if (#name <= 0) then
      name = backup_name
    end
    --print(get_ppd(cursor))
    declaration = declaration .. name .. " {"

    visit_children(cursor, function(cursor)
      local kind = clang.clang_getCursorKind(cursor)
      if kind == clang.CXCursor_FieldDecl then
        declaration = declaration .. "\n  " .. get_struct_field_declaration(cursor)
      end
      return clang.CXChildVisit_Continue
    end)

    if declaration:sub(-1) ~= "{" then
      declaration = declaration .. "\n"
    end

    declaration = declaration .. "};"

    return declaration
  end

  local function get_enum_constant_declaration(cursor)
    local declaration = get_ppd(cursor) .. ","

    add_type_if_necessary(clang.clang_getCursorType(cursor))

    return declaration
  end

  local function get_enum_string(cursor, backup_name)
    local declaration = "enum "

    local name = get_cs(cursor)
    if (#name <= 0) then
      name = backup_name
    end
    declaration = declaration .. name .. " {"

    visit_children(cursor, function(cursor)
      local kind = clang.clang_getCursorKind(cursor)
      if kind == clang.CXCursor_EnumConstantDecl then
        declaration = declaration .. "\n  " .. get_enum_constant_declaration(cursor)
      end
      return clang.CXChildVisit_Continue
    end)

    if declaration:sub(-1) ~= "{" then
      declaration = declaration .. "\n"
    end

    declaration = declaration .. "};"

    return declaration
  end

  local function get_union_field_declaration(cursor)
    local declaration = get_ppd(cursor) .. ";"

    add_type_if_necessary(clang.clang_getCursorType(cursor))

    return declaration
  end

  local function get_union_string(cursor, backup_name)
    local declaration = "union "

    local name = get_cs(cursor)
    if (#name <= 0) then
      name = backup_name
    end
    --print(get_ppd(cursor))
    declaration = declaration .. name .. " {"

    visit_children(cursor, function(cursor)
      local kind = clang.clang_getCursorKind(cursor)
      if kind == clang.CXCursor_FieldDecl then
        declaration = declaration .. "\n  " .. get_union_field_declaration(cursor)
      end
      return clang.CXChildVisit_Continue
    end)

    if declaration:sub(-1) ~= "{" then
      declaration = declaration .. "\n"
    end

    declaration = declaration .. "};"

    return declaration
  end

  function add_type_if_necessary(cxtype, backup_name)
    if cxtype.kind == clang.CXType_Pointer then
      add_type_if_necessary(clang.clang_getPointeeType(cxtype), backup_name)
      return
    elseif cxtype.kind == clang.CXType_ConstantArray then
      add_type_if_necessary(clang.clang_getArrayElementType(cxtype), backup_name)
      return
    elseif cxtype.kind == clang.CXType_FunctionProto then
      add_type_if_necessary(clang.clang_getResultType(cxtype))
      for i=0,clang.clang_getNumArgTypes(cxtype)-1 do
        add_type_if_necessary(clang.clang_getArgType(cxtype, i))
      end
      return
    end
    local cursor = clang.clang_getTypeDeclaration(cxtype)
    local hash = clang.clang_hashCursor(cursor)
    if added_types[hash] ~= nil then
      return
    end
    added_types[hash] = true
    if clang.clang_Cursor_isAnonymous(cursor) ~= 0 then
      return
    end
    local kind = clang.clang_getCursorKind(cursor)
    if kind == clang.CXCursor_NoDeclFound then
    elseif kind == clang.CXCursor_TypedefDecl then
      table.insert(types, get_typedef_string(cursor))
    elseif kind == clang.CXCursor_StructDecl then
      table.insert(types, get_struct_string(cursor, backup_name))
    elseif kind == clang.CXCursor_EnumDecl then
      table.insert(types, get_enum_string(cursor, backup_name))
    elseif kind == clang.CXCursor_UnionDecl then
      table.insert(types, get_union_string(cursor, backup_name))
    else
      error(get_ls(clang.clang_getCursorKindSpelling(kind)))
    end
    --print(get_ls(clang.clang_getTypeSpelling(cxtype)))
  end

  local function get_function_declaration(cursor)
    local declaration = get_ppd(cursor) .. ";"
    add_type_if_necessary(clang.clang_getCursorResultType(cursor))
    for i=0,clang.clang_Cursor_getNumArguments(cursor)-1 do
      add_type_if_necessary(clang.clang_getCursorType(clang.clang_Cursor_getArgument(cursor, i)))
    end
    return declaration
  end

  visit_children(cursor, function(cursor)
    local kind = clang.clang_getCursorKind(cursor)
    if kind == clang.CXCursor_FunctionDecl then
      if functions_filter == nil or functions_filter[get_cs(cursor)] ~= nil then
        table.insert(functions, get_function_declaration(cursor))
      end
    end
    return clang.CXChildVisit_Continue
  end)
  return table.concat({table.concat(types, "\n"), table.concat(functions, "\n")}, "\n")
end

return generate_cdef
