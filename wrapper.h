#include <clang-c/Index.h>

typedef enum CXChildVisitResult (*wrapp_f)(CXCursor *cursor);

enum CXChildVisitResult call_wrapper(CXCursor cursor, CXCursor parent, CXClientData client_data);
void *wrap_function(wrapp_f f);
