local ffi = require("ffi")

local used_cdefs = {}

local function secure_cdef(cdef)
  local blocks = {}
  for line in cdef:gmatch("[^\n\r]+") do
    if line:match("^ ") or line == "};" then
      blocks[#blocks] = blocks[#blocks] .. "\n" .. line
    else
      table.insert(blocks, line)
    end
  end
  for _, block in ipairs(blocks) do
    if used_cdefs[block] == nil then
      used_cdefs[block] = true
      --print(block)
      ffi.cdef(block)
    end
  end
end
return secure_cdef
